(ns fanega.handler.example
  (:require [ataraxy.core :as ataraxy]
            [ataraxy.response :as response]
            [clojure.java.io :as io]
            [duct.database.sql]
            [integrant.core :as ig]))

(defmethod ig/init-key ::get [_ {:keys [db]}]
  (fn [{[_] :ataraxy/result}]
    [::response/ok (io/resource "fanega/handler/example/example.html")]))
