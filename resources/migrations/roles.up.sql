CREATE TABLE roles (
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(10) NOT NULL
);

INSERT INTO roles VALUES
	(1, 'public'),
	(2, 'mod'),
	(3, 'admin');
