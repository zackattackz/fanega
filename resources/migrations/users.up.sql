CREATE TABLE users (
  id       BIGSERIAL PRIMARY KEY,
  username VARCHAR(32) NOT NULL UNIQUE,
  password CHAR(86) NOT NULL,
  salt     CHAR(22) NOT NULL,
  role_id  BIGSERIAL
);
