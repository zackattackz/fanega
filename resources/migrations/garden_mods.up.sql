CREATE TABLE garden_mods (
  user_id   BIGSERIAL,
  garden_id BIGSERIAL,
  PRIMARY KEY(user_id, garden_id)
);
